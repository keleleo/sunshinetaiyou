﻿using System;
using System.IO;
using SunshineTaiyou.Taiyou;
using System.Reflection;
using TaiyouFramework;

namespace SunshineTaiyou
{
    class Program
    {
        public static TaiyouContext mainContext = new TaiyouContext("Main Context");

        // Main entry point
        static void Main(string[] args)
        {
            CompileScripts();

        }

        public static void CompileScripts()
        {
            // Create "Core" namespace
            mainContext.AddNamespace("Core");

            DirectoryInfo files = new DirectoryInfo("program");
            string currentDir = Path.Combine("program", Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase));

            foreach(FileInfo info in files.GetFiles("*.tasm", SearchOption.AllDirectories))
            {
                string AssemblyName = Path.GetRelativePath(currentDir, info.FullName).Replace("../", "");

                Console.WriteLine($"-- Loading assembly \"{AssemblyName}\"...");

                TaiyouAssembly newAssembly = new TaiyouAssembly(mainContext, new SourceAssembly(File.ReadAllText(info.FullName), AssemblyName));
                
                Console.WriteLine("Compiling assembly...");
                newAssembly.Compile();

            }


        }
    }
}
