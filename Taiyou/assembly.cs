using System.Collections.Generic;
using System;
using TaiyouFramework;

namespace SunshineTaiyou.Taiyou
{
    // Class for holding Taiyou Assembly
    public class TaiyouAssembly : TaiyouObj
    {
        public SourceAssembly source;
        public bool IsCompiled = false;

        public TaiyouAssembly(TaiyouContext pContext, SourceAssembly pSource)
        {
            context = pContext; source = pSource;

        }

        /// <summary>Compiles source code</summary>
        public void Compile()
        {
            string[] CodeSplit = source.SourceCode.Split(new char[] { '\n', '\r' }, System.StringSplitOptions.RemoveEmptyEntries);

            // Breaks code into lines
            foreach (string line in CodeSplit)
            {
                string Line = line.Trim();
                string Cmdlet = Line.Split(" ")[0];
                string AnalysisString = Line.Remove(0, Cmdlet.Length);
                List<string> ArgumentsList = GetArgumentsList(AnalysisString);



            }


            IsCompiled = true;
        }

        /// <summary>Convert analysis string into argument list</summary>
        /// <param name="AnalysisString">Analysis <c>String<c/></param>
        private List<string> GetArgumentsList(string AnalysisString)
        {
            bool StringLiteralStarted = false;
            bool IgnoreToken = false;
            string CurrentResult = "";
            List<string> FinalResult = new List<string>();

            foreach (char Char in AnalysisString)
            {
                // Ignore token, token
                if (Char == '\\')
                {
                    IgnoreToken = true;
                    continue;
                }

                // String Enclosure token
                if (Char == '\"' && !IgnoreToken)
                {
                    if (!StringLiteralStarted)
                    {
                        StringLiteralStarted = true;
                        continue;
                    }

                    StringLiteralStarted = false;
                    FinalResult.Add(CurrentResult);
                    CurrentResult = "";
                    continue;

                }

                // Add token to be ignored
                if (IgnoreToken) { IgnoreToken = false; CurrentResult += Char; continue; }

                if (StringLiteralStarted) { CurrentResult += Char; }

            }


            return FinalResult;

        }


    }

    // Class for holding assembly source code information
    public class SourceAssembly : TaiyouObj
    {
        public string SourceCode = "";
        public string AssemblyName = "";

        public override string ToString()
        {
            return AssemblyName;
        }

        public SourceAssembly(string pSourceCode, string pAssemblyName) { SourceCode = pSourceCode; AssemblyName = pAssemblyName; }
    }

}