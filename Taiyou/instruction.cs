namespace SunshineTaiyou.Taiyou
{
    public abstract class TaiyouInstruction
    {
        public string Name = "unnamed";

        public virtual void Run(object[] Arguments) { }

        public override string ToString() { return Name; }
        
    }

}