using System.Collections.Generic;
using System;

namespace TaiyouFramework
{
    public abstract class TaiyouObj
    {
        public TaiyouContext context;
    }
    
    public class TaiyouContext
    {
        #region Namespace Manipulation
        public Dictionary<string, Namespace> AvailableNamespaces = new Dictionary<string, Namespace>();
        
        public void AddNamespace(string NamespaceName)
        {
            // Return if namespace already exists
            if (AvailableNamespaces.ContainsKey(NamespaceName)) { return; }
            // Add namespace if it doesn't exist
            AvailableNamespaces.Add(NamespaceName, new Namespace(this, NamespaceName));
        }
        #endregion

        public TaiyouContext(string pContextName) { ContextName = pContextName; }
 
        public string ContextName;

        public override string ToString()
        {
            return ContextName;
        }


    }
}